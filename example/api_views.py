from django.http import JsonResponse
from .models import Blog, Comment
from common.json import ModelEncoder
import json

class CommentListEncoder(ModelEncoder):
    model = Comment
    properties = ["content"]

class BlogListEncoder(ModelEncoder):
    model = Blog
    properties = ["title"]

class BlogDetailEncoder(ModelEncoder):
    model = Blog
    properties = ["title", "content",]

def list_blogs(request):
    # Get all of the blogs
    blogs = Blog.objects.all()
    # Return the blogs in a JsonResponse
    return JsonResponse({"blogs": blogs}, encoder=BlogListEncoder)

def show_blog_detail(request, pk):
    # Get the blog
    blog = Blog.objects.get(pk=pk)
    # Return the JsonResponse
    return JsonResponse({"blog": blog}, encoder=BlogDetailEncoder)

def create_comment(request, blog_id):
    if request.method == "POST":
        blog = Blog.objects.get(id=blog_id)
        content = json.loads(request.body)
        Comment.objects.create(
            content=content["comment"],
            blog=blog,
        )
        return JsonResponse({"message": "created"})
